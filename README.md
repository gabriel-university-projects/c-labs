# Compiling and Running

This is a guide to compiling and running the code in this repository.

## Commands

In the project directory, you can run:

### `gcc [filename].c -o [name of output file]`

This will compile the c file into an executable output file that can be run in the terminal.

#### Example of use

gcc Arrays.c -o output.out

### `./[name of output file]`

This will run the executable output file in the terminal.

#### Example of use

./output.out